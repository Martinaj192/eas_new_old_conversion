﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace XDocFunc
{
    class XmlDocFunctions
    {

        public static List<XmlElement> getElementsByClassName(string type, string classNm, XmlDocument doc)
        {
            XmlNodeList list = doc.GetElementsByTagName(type);
            List<XmlElement> elements = new List<XmlElement>();
            
            foreach (XmlNode n in list)
            {
                XmlElement element = (XmlElement)n;
                if (element.HasAttribute("class") && element.GetAttribute("class") == classNm)
                    elements.Add(element);
            }

            return elements;
        }

        public static List<XmlElement> getElementsByAttribute(string type, string attrName, string attrValue, XmlDocument doc)
        {
            XmlNodeList list = doc.GetElementsByTagName(type);
            List<XmlElement> elements = new List<XmlElement>();

            foreach (XmlNode n in list)
            {
                XmlElement element = (XmlElement)n;
                if (element.HasAttribute(attrName) && element.GetAttribute(attrName) == attrValue)
                    elements.Add(element);
            }

            return elements;
        }
    }
}
